// console.log("Hello world!")

// [SECTION] Assignment Operator
 // Basic Assignment Operator (=)
 // The assignment operator assign the value of the right hand operand to a variable

  let assignmentNumber = 8;
  console.log("The value of the assignNumber variable: " + assignmentNumber)



// [SECTION] Arithmetic Operations

let x = 200;
let y = 18;

console.log("x:" + x);
console.log("y:" + y);
console.log("-----" );

//Addition
 let sum = x + y;
 console.log("Result of the addition operator: " + sum)

//Subtraction
 let difference = x - y;
 console.log("Result of the subtraction operator: " + difference)

 //Multiplication
 let product = x * y;
 console.log("Result of the multiplication operator: " + product)

 //Division
 let quotient = x / y;
 console.log("Result of the division operator: " + quotient)

// Modulo
 let modulo = x % y;
 console.log("Result of modulo operator:" + modulo)

 // Continuation of assignment operator

 // Addition Assignment Operator
	 // assignmentNumber = assignmentNumber + 2;
	 // console.log(assignmentNumber);

	 // short method
	 assignmentNumber+=2;
	 console.log("Result of Addition Assignment Operator: " + assignmentNumber);


// Subtraction Assignment Operator
	 // assignmentNumber = assignmentNumber - 3;
	 // console.log(assignmentNumber);

	assignmentNumber -= 3;
    console.log("Result of Subtraction Assignment Operator: " + assignmentNumber)


 // Multiplication Assignment Operator
	 // assignmentNumber = assignmentNumber * 3;
	 // console.log(assignmentNumber);

	assignmentNumber *= 2;
    console.log("Result of Multiplication Assignment Operator: " + assignmentNumber)

 // Division Assignment Operator
	 // assignmentNumber = assignmentNumber / 3;
	 // console.log(assignmentNumber);

	assignmentNumber /= 2;
    console.log("Result of Division Assignment Operator: " + assignmentNumber)


 // [SECTION] PEMDAS (Order of Operations)
 // Multiple Operators

 let mdas = 1 + 2 - 3 * 4 / 5
 console.log(mdas)

 	// The operations done In the following orders
 	// 	1. 3 * 4 = 12 | 1 + 2 - 12 / 5
		// 2. 12 / 5 = 2.4 | 1 + 2 - 2.4
		// 3. 1. 2 = 3 | 3 - 2.4
		// 4. 3 - 2.4 = 0.6
	

let pemdas = (1+(2-3))+(4/5);
console.log("Result of PEMDAS operator: " + pemdas)


// hierarchy
// combination of multiple arithmetic operators will follow the pemdas rule

/*
	1. parenthesis
	2. exponent
	3. multiplication / division
	4. addition / subtraction

	NOTE: will also follow the left to right side
*/

// [SECTION] Increment(add) and Decrement(subtract)

// Operators that add or subtract values by 1 and reassign the value of the variable where increment (++) /decrement(--) was applied


let z = 1;

//pre-increment
let increment = ++z;
console.log("Result of pre-increment: " + increment); //2
console.log("Result of pre-increment of z: " + z); //2

// post-increment
increment = z++;

console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment of z: " + z); //2

increment = z++;

console.log("Result of post-increment: " + increment); //2
console.log("Result of post-increment of z: " + z); //2


// pre-decrement

let decrement=  --z;
console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement of z: " + z);

// post-decrement

decrement = z--;

console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement of z: " + z);


// [SECTION] Type Coercion
// is the automatic conversion of values from one data type to another
	
	//combination of number and string data type will result to string
	let numA = 10; //number
	let numB = "12"; //string

	let coercion = numA+numB;
	console.log(coercion)

	// 
	let numC = 16;
	let numD = 16;
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion)

	//combination of number and boolean data will result to a number
	let numX = 10;
	let numY = true;
	//boolean values are
	// true =1;
	// false = 0

	coercion = numX+numY;
	console.log(coercion)
	console.log(typeof coercion)


	let num1 = 1;
	let num2 = false;
	coercion = num1 + num2
	console.log(coercion)
	console.log(typeof coercion);


//[SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands
 // After evaluation, it returns a boolean value

// equality operator [==] (read as equal to)
// compares the value, but not the data type 
console.log("----------")
console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true //false boolean is equal to zero


let juan = "juan"
console.log('juan' == 'juan'); //true
console.log('juan' == 'Juan'); //false // equality operator is strict with letter casing
console.log(juan == "juan"); //true



// inequality operator [!=] (also read as not equal)

console.log("----------")
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false

console.log('juan' != 'juan'); //false
console.log('juan' != 'Juan'); //true 
console.log(juan != "juan"); //false




// [SECTION] Relational Operators
// some comparison operators to check whether one value is greater or less that the other value
console.log("----------")

let a = 50;
let b = 65;

// GT - greater than operator (>) 
let isGreaterThan = a > b; // 50 > 65 = false
console.log(isGreaterThan);


// LT - less than operator (<)
let isLessThan = a < b; // 50 > 65 = true
console.log(isLessThan);	


// GTE -  greater than or equal (>=)
let isGTorEqual = a >= b; //false
console.log(isGTorEqual);

// LTE - less than or equal (>=)
let isLTorEqual = a <= b; //true
console.log(isLTorEqual);


// Forced Coercion
let num = 30
let numStr = '30';
console.log(num > numStr);


let str = 'twenty';
console.log(num >= str);



console.log("----------")

// Logical AND Operator (&&)
// operator requires all/both are true


let isLegalAge = true;
let isRegistered = false;

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical and Operator: " + allRequirementsMet); //false


// Logical OR Operator
// requires only 1 true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet); //true

// Logical NOT Operator (!)
// Returns the opposite value

let someRequirementsNotMet = !isLegalAge;
console.log("Logical NOT Operator: " + someRequirementsNotMet);//true